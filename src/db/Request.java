package db;

import java.io.*;
import java.util.*;

import rmi.*;

public class Request implements Serializable {
	static final long serialVersionUID = 6L;
	private int order;
	private int id;
	private int orderID;
	private String name;
	private String pass;
	private Post post;
	private Message message;
	private String username;
	private ClientInterface c;
	private String authURL;
	private FacebookRestClient rest;
	private byte[] imageBytes = null;

	public FacebookRestClient getFacebookRestClient(){
		return this.rest;
	}

	public void setFacebookRestClient(FacebookRestClient tmp){
		this.rest=tmp;
	}

	public String getauthURL(){
		return this.authURL;
	}

	public void setauthURL(String tmp){
		this.authURL=tmp;
	}

	public void setID(int id){
		this.id = id;
	}
	public void setOrderID(int id){
		this.orderID = id;
	}
	public void setOrder(int order){
		this.order = order;
	}
	public void setName(String name){
		this.name = name;
	}
	public void setPass(String pass){
		this.pass = pass;
	}
	public void setPost(Post post){
		this.post = post;
	}
	public void setMessage(Message message){
		this.message = message;
	}
	public void setUsername(String username){
		this.username = username;
	}
	public void setClientInterface(ClientInterface c){
		this.c = c;
	}
	
	public int getID(){
		return this.id;
	}
	public int getOrderID(){
		return this.orderID;
	}
	public String getName(){
		return this.name;
	}
	public String getPass(){
		return this.pass;
	}
	public int getOrder(){
		return this.order;
	}
	public Post getPost(){
		return this.post;
	}
	public Message getMessage(){
		return this.message;
	}
	public String getUsername(){
		return this.username;
	}
	public ClientInterface getClientInterface(){
		return this.c;
	}
	public void setImageBytes(byte[] imageBytes){
		this.imageBytes = imageBytes;
	}
	public byte[] getImageBytes(){
		return this.imageBytes;
	}

}