package db;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import asciiart.*;


public class Post implements Serializable{
	static final long serialVersionUID = 2L;
	private String post;
	private String fbPostId;
	private String image;
	private String imagePath;
	private byte[] imageBytes;
	private String user;
	private ConcurrentLinkedQueue<String> comments;
	private String title;
	private Date created;
	private Date edited;
	private int id;

	public Post(String title, String post, Date date){
		this.created = date;
		this.title = title;
		this.post = post;
		this.image = null;
		this.imagePath = null;
		this.imageBytes = null;
		this.edited = null;
		this.comments = new ConcurrentLinkedQueue<>();
		this.fbPostId = null;
	}

	public void setID(int id){
		this.id = id;
	}
	public void setUser(String user){
		this.user = user;
	}
	public void setPost(String post){
		this.post = post;
	}
	public void setImage(String image){
		this.image = image;
	}
	public void setImagePath(String imagePath){
		this.imagePath = imagePath;
	}
	public void setImageBytes(byte[] imageBytes){
		this.imageBytes = imageBytes;
		BufferedOutputStream bos = null;
		File file = null;
		try{
			file = new File("images/"+this.user+"/"+this.id);
			File parent = file.getParentFile();
			if(!parent.exists() && !parent.mkdirs()){
    			throw new IllegalStateException("Couldn't create dir: " + parent);
			}
			if(!file.exists()){
  				file.createNewFile();
  			}
			FileOutputStream fos = new FileOutputStream(file);
			bos = new BufferedOutputStream(fos); 
			bos.write(imageBytes);
		}catch (Exception e){
			e.printStackTrace();
		}finally {
			if(bos != null) {
				try  {
          			//flush and close the BufferedOutputStream
					bos.flush();
					bos.close();
					this.setImagePath("images/"+this.user+"/"+this.id);
				} catch(Exception e){}
			}
		}

		ASCIIConverter conv = new ASCIIConverter();
		try{
			this.setImage(conv.convertAndResize(file.getPath()));
			System.out.println("Image added at: " + file.getPath());
		}catch(Exception e){
			System.out.println("Error on Image: " + e.getMessage());
		}
	}
	public void setComments(ConcurrentLinkedQueue<String> comments){
		this.comments = comments;
	}
	public void setTitle(String title){
		this.title = title;
	}
	public void addComment(String c){
		this.comments.offer(c);
	}

	public String getUser(){
		return this.user;
	}
	public String getPost(){
		return this.post;
	}
	public String getImage(){
		return this.image;
	}
	public String getImagePath(){
		return this.imagePath;
	}
	public byte[] getImageBytes(){
		return this.imageBytes;
	}
	public ConcurrentLinkedQueue<String> getComments(){
		return this.comments;
	}
	public String getTitle(){
		return this.title;
	}
	public Date getCreated(){
		return this.created;
	}
	public Date getEdited(){
		return this.edited;
	}
	public int getID(){
		return this.id;
	}

	public void editPost(Post tmp, byte[] imageBytes){
		this.title = tmp.getTitle();
		this.post = tmp.getPost();
		this.edited = tmp.getEdited();
		if(imageBytes!=null){
			this.setImageBytes(imageBytes);
		}
	}

	public void setFbPostId(String tmp){
		this.fbPostId=tmp;
	}

	public String getFbPostId(){
		return this.fbPostId;
	}
	
}