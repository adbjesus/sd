package db;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;

public class Message  implements Serializable{
	static final long serialVersionUID = 3L;
	private String title;
	private String message;
	private Date date;
	private int id;
	private String sender;

	public Message(String title,String message, Date date){
		this.date = new Date();
		this.message = message;
		this.title = title;
		this.date = date;
		this.sender = sender;
	}

	public void setID(int id){
		this.id = id;
	}
	public void setTitle(String title){
		this.title = title;
	}
	public void setMessage(String message){
		this.message = message;
	}
	public void setDate(Date date){
		this.date = date;
	}
	public void setSender(String tmp){
		this.sender = tmp;
	}

	public String getTitle(){
		return this.title;
	}
	public String getMessage(){
		return this.message;
	}
	public Date getDate(){
		return this.date;
	}
	public int getID(){
		return this.id;
	}
	public String getSender(){
		return this.sender;
	}
}
