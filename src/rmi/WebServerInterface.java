package rmi;

import db.User;

public interface WebServerInterface extends java.rmi.Remote{
	public void addOnlineUser(User user) throws java.rmi.RemoteException;
	public void removeOnlineUser(User user) throws java.rmi.RemoteException;
}