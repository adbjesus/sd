package server;

import java.io.*;
import java.net.*;
import java.util.*;
import java.rmi.*;
import java.rmi.registry.*;
import java.util.concurrent.*;
import rmi.*;
import db.*;

public class rmiServer 
extends java.rmi.server.UnicastRemoteObject
implements RMIServerInterface{

	public rmiServer() throws java.rmi.RemoteException{}

	public Reply login(Request req) throws java.rmi.RemoteException{
		Reply rep = Server.login(req);
		/*Login sucess*/
		User user = Server.userExists(req.getName());
		if(rep.getBool()){
			user.addRMI(req.getClientInterface());
		}
		return rep;
	}

	public void silentLogin(Request req) throws java.rmi.RemoteException{
		User user = Server.userExists(req.getUsername());
		user.addRMI(req.getClientInterface());
		user.setLogged(true);
		Server.addOnlineUser(user);
	}

	public Reply registar(Request req) throws java.rmi.RemoteException{
		Reply rep = Server.register(req);
		return rep;
	}

	public Reply listarAmigos(Request req) throws java.rmi.RemoteException{
		Reply rep = Server.listFriends(req);
		return rep;
	}

	public Reply consultarPosts(Request req) throws java.rmi.RemoteException{
		Reply rep  = Server.listPosts(req);
		return rep;
	}

	public void novoPost(Request req) throws java.rmi.RemoteException{
		Reply rep = Server.newPost(req);
		//addServed(req.getOrderID());
		Reply ack = new Reply();
		ack.setOrderID(req.getOrderID());
		req.getClientInterface().ack(ack);
	}

	public void editarPost(Request req) throws java.rmi.RemoteException{
		Reply rep = Server.editPost(req);
		//addServed(req.getOrderID());
		Reply ack = new Reply();
		ack.setOrderID(req.getOrderID());
		req.getClientInterface().ack(ack);
	}

	public void apagarPost(Request req) throws java.rmi.RemoteException{
		Reply rep = Server.deletePost(req);
		//addServed(req.getOrderID());
		Reply ack = new Reply();
		ack.setOrderID(req.getOrderID());
		req.getClientInterface().ack(ack);
	}

	public void enviarMensagem(Request req) throws java.rmi.RemoteException{
		Reply rep = Server.sendMessage(req);
		//addServed(req.getOrderID());
		Reply ack = new Reply();
		ack.setOrderID(req.getOrderID());
		req.getClientInterface().ack(ack);
	}

	public Reply consultarMensagens(Request req) throws java.rmi.RemoteException{
		Reply rep = Server.listMessages(req);
		return rep;
	}

	public void responderPost(Request req) throws java.rmi.RemoteException{
		Reply rep = Server.replyPost(req);
		//addServed(req.getOrderID());
		Reply ack = new Reply();
		ack.setOrderID(req.getOrderID());
		req.getClientInterface().ack(ack);
	}

	public Reply adicionarAmigo(Request req) throws java.rmi.RemoteException{
		Reply rep = Server.addFriend(req);
		//addServed(req.getOrderID());
		Reply ack = new Reply();
		ack.setOrderID(req.getOrderID());
		req.getClientInterface().ack(ack);
		return rep;
	}

	public void removerAmigo(Request req) throws java.rmi.RemoteException{
		Reply rep = Server.removeFriend(req);
		//addServed(req.getOrderID());
		Reply ack = new Reply();
		ack.setOrderID(req.getOrderID());
		req.getClientInterface().ack(ack);
	}

	public Reply listOnlineUsers(Request req) throws java.rmi.RemoteException{
		Reply rep = Server.listOnlineUsers();
		return rep;
	}

	public Reply logout(Request req) throws java.rmi.RemoteException{
		Reply rep = Server.logout(req);
		User user = Server.userExists(req.getUsername());
		user.removeRMI(req.getClientInterface());
		return rep;
	}

	public void subscribe(ClientInterface c) throws java.rmi.RemoteException{
		//this.c = c;
		return;
	}

	public void saveFiles() throws java.rmi.RemoteException{
		Server.saveFiles();
	}

	public void setFacebookRestClient(Request req) throws java.rmi.RemoteException{
		Server.setFacebookRestClient(req);
		return;
	}

	public User getUser(String user) throws java.rmi.RemoteException{
		return Server.userExists(user);
	}

	public void subscribeWS(WebServerInterface ws) throws java.rmi.RemoteException {
		Server.ws = ws;
	}
}
