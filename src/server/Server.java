/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import java.io.*;
import java.net.*;
import java.util.*;
import java.rmi.*;
import java.rmi.registry.*;
import java.util.concurrent.*;
import rmi.*;
import db.*;

public class Server {
	public static ConcurrentLinkedQueue<Connection> logged = new ConcurrentLinkedQueue<>();
	public static ConcurrentLinkedQueue<User> users;
	public static ConcurrentLinkedQueue<Request> delayed;
	public static ConcurrentLinkedQueue<User> online = new ConcurrentLinkedQueue<>();
	public static WebServerInterface ws = null;
	public static String USER_FILE = "users.obj";
	public static String DELAYED_FILE = "delayed.obj";
	public static int TCP_PORT = 7000;
	public static int UDP_PORT = 4444;
	public static int RMI_PORT = 8000;
	
	public static ConcurrentLinkedQueue readFile(String s, ConcurrentLinkedQueue c){
		try{ 
			FileInputStream fs = new FileInputStream(s);
			ObjectInputStream is = new ObjectInputStream(fs);
			c = (ConcurrentLinkedQueue<User>)is.readObject();
			is.close();
			fs.close();
		} catch (Exception e){
			e.printStackTrace();
			c = new ConcurrentLinkedQueue<>();
		}
		return c;
	}

	public static void init(){
		users = readFile(USER_FILE,users);
		delayed = readFile(DELAYED_FILE,delayed);
		Iterator<User> it = users.iterator();
		CheckFacebookComments tmp = new CheckFacebookComments();
		tmp.start();
		while(it.hasNext()){
			it.next().startStreams();
		}
		setUpDelayed();
		/*users = new ConcurrentLinkedQueue<>();
		delayed = new ConcurrentLinkedQueue<>();*/
	}

	public static void setupServer(){
		String s;
		Connection con;
		Thread t;

		System.getProperties().put("java.security.policy", "../security/security.policy");
		System.setSecurityManager(new RMISecurityManager());
		try{
			System.out.println("A escuta no porto "+TCP_PORT);
			ServerSocket listenSocket = new ServerSocket(TCP_PORT);
			LocateRegistry.createRegistry(RMI_PORT);
			RMIServerInterface rmiConImpl = new rmiServer();
			Naming.rebind("//localhost:"+RMI_PORT+"/rmiServer",rmiConImpl);

			while(true){
				Socket clientSocket = listenSocket.accept();
				System.out.println("Client_Socket (created at accept()) = " + clientSocket);

				try{
					con = new Connection(clientSocket);
					t = new Thread(con);
					t.start();
					logged.add(con);
				} catch (Exception e){
					System.out.println("Exception: " + e.getMessage());
				}
			}
		}catch(IOException e){
			System.out.println("Listen:" + e.getMessage());
			System.out.println(e.getStackTrace());
		}
		online = new ConcurrentLinkedQueue<>();
	}

	public static void main(String args[]){
		if(args.length!=2){
			System.out.println("java Server [backup|primary] otherIP ");
			return;
		}
		String other_ip = args[1];
		int n = 0;
		

		if(args[0].compareTo("backup")==0){
			backup(other_ip);
		}
		else if(args[0].compareTo("primary")==0){
			primary(other_ip);
		}

		ShutdownHook shHook = new ShutdownHook();
		shHook.attachShutDownHook();
	}

	public static void primary(String other_ip){
		init();
		new ConfirmThread(other_ip);
		setupServer();
	}

	public static void backup(String other_ip){
		System.out.println("Backup server starting");
		int n=0;
		DatagramSocket socket = null;
		while(n<5){
			try{
				socket = new DatagramSocket(UDP_PORT);
				socket.setSoTimeout(3000);
				System.out.print("Receiving ");
				while(true){
					byte[] buf = new byte[256];
					DatagramPacket ping   = new DatagramPacket(buf,buf.length);
					socket.receive(ping);	
					n=0;
					System.out.print(".");
				}

			}
			catch(SocketTimeoutException e){
				System.out.println("Lost connection");
				n++;
			}	
			catch(SocketException e){System.out.println("Socket1:" + e.getMessage());}
			catch(IOException e){System.out.println("IOException:" + e.getMessage());}

			if(socket!=null)
				socket.close();

		}
		primary(other_ip);
	}

	public static User userExists(String name) {
		Iterator<User> it;
		it = Server.users.iterator();
		User tmp;

		while(it.hasNext()){
			tmp = it.next();
			System.out.println(tmp.getName() + "  " + name);
			if(tmp.getName().compareTo(name)==0) return tmp;
		}
		return null;
	}

	public static void setUpDelayed(){
		Iterator<Request> it = delayed.iterator();
		while(it.hasNext()){
			Request req = it.next();
			long t = req.getPost().getCreated().getTime()- new Date().getTime();
			Timer tim = new Timer();
			if(t>0){
				tim.schedule(new addNewPost(req,tim,true),t);
			}
			else{
				tim.schedule(new addNewPost(req,tim,true), 0);
			}
		}
	}

	public static void noti(String tmp, User user) {
		Reply rep = new Reply();
		rep.setTitle("Notify");
		rep.setReport(tmp);
		Iterator<User> uit = user.getFollowers().iterator();
		Iterator<ObjectOutputStream> oit;
		Iterator<ClientInterface> cit;
		ObjectOutputStream oos;
		ClientInterface ci;
		User usr;
		while(uit.hasNext()){
			usr = uit.next();
			oit = usr.getStream().iterator();
			while(oit.hasNext()){
				oos = oit.next();
				try{
					oos.writeObject(rep);
				} catch (IOException e){}
			}
			cit = usr.getClients().iterator();
			while(cit.hasNext()){
				System.out.println("FOUND RMI");
				ci = cit.next();
				try{
					ci.noti(rep);
					System.out.println("SENT NOTI");
				} catch (RemoteException e){
					e.printStackTrace();
				}
			}

		}
	}

	public static void saveFiles(){
		try{
			FileOutputStream fs = new FileOutputStream(Server.USER_FILE);
			ObjectOutputStream oS = new ObjectOutputStream(fs);
			oS.writeObject(Server.users);
			fs.close();
			oS.close();
			fs = new FileOutputStream(Server.DELAYED_FILE);
			oS = new ObjectOutputStream(fs);
			oS.writeObject(Server.delayed);
			fs.close();
			oS.close();
		} catch (Exception e){
			e.printStackTrace();
		}
	}

	public static void notiUser(String tmp, User usr) {
		Reply rep = new Reply();
		Iterator<ObjectOutputStream> oit;
		Iterator<ClientInterface> cit;
		ObjectOutputStream oos;
		rep.setTitle("Notify");
		rep.setReport(tmp);
		oit = usr.getStream().iterator();
		while(oit.hasNext()){
			oos = oit.next();
			try{
				oos.writeObject(rep);
			} catch (IOException e){}
		}
		cit = usr.getClients().iterator();
		while(cit.hasNext()){
			try{
				cit.next().noti(rep);
			} catch (RemoteException e){
				e.printStackTrace();
			}
		}
	}

	public static void addOnlineUser(User user){
		Server.online.add(user);
		try{
			ws.addOnlineUser(user);
		} catch (Exception e){
			System.out.println("Web server not connected so can't add online user");
		}
	}

	public static void removeOnlineUser(User user){
		Server.online.remove(user);
		try{
			ws.removeOnlineUser(user);
		} catch (Exception e){
			System.out.println("Web server not connected so can't remove online user");
		}
	}


	public static void setFacebookRestClient(Request req){
		User user = Server.userExists(req.getUsername());
		FacebookRestClient rest = req.getFacebookRestClient();

		rest.newService(false);
		user.setFacebookRestClient(rest);
		return;
	}


	public static Reply login(Request req){
		String name;
		String pass;
		User user;
		Boolean logged_in = false;
		Reply rep = new Reply();
		FacebookRestClient rest = req.getFacebookRestClient();
		name = req.getName();
		pass = req.getPass();
		user = Server.userExists(name);
		if(user!=null && user.getPass().compareTo(pass)==0){
			System.out.println("LALA");
			//inserir conta do face
			if(rest!=null){
                            rest.newService(false);
                            user.setFacebookRestClient(rest);
                        }
			//Continuar operaçoes
			user.setLogged(true);
			Server.addOnlineUser(user);
			rep.setReport("User logged in\n");
			logged_in = true;
		}
		else if(user!=null){
			rep.setReport("Error: Wrong password\n");
		}
		else{
			rep.setReport("Error: User doesn't exist\n");
		}
		rep.setTitle("Login");
		rep.setBool(logged_in);
		rep.setUser(user);
		Server.saveFiles();
		Server.noti("User "+ user.getName()+ " logged in",user);
		System.out.println("User "+name+" login attempt: "+logged_in);

		return rep;		
	}

	public static Reply register(Request req){
		String name;
		String pass;
		Reply rep = new Reply();

		name = req.getName();
		pass = req.getPass();
		rep.setTitle("Registar");

		if(Server.userExists(name)!=null){
			rep.setBool(false);
			rep.setReport("Error: User "+name+" already registered\n");
		}
		else{
			Server.users.add(new User(name,pass,null));
			rep.setBool(true);
			rep.setReport("User "+name+" registered\n");
		}
		Server.saveFiles();

		System.out.println("User "+name+" register attempt: "+rep.getBool());
		return rep;
	}

	public static Reply listFriends(Request req){
		Reply rep = new Reply();
		User[] users;
		String[] usersname;

		User user = userExists(req.getUsername());

		rep.setTitle("listarAmigos");

		users = user.getFriends().toArray(new User[0]);
		usersname = new String[users.length];
		for(int i=0;i<users.length;i++){
			usersname[i]=users[i].getName();
		}
		rep.setUsers(usersname);
		System.out.println("Sending friend list, size: " + users.length);

		return rep;
	}

	public static Reply listPosts(Request req) {
		String name;
		Reply rep = new Reply();
		User user = userExists(req.getUsername());
		User friend;
		
		name = req.getName();
		friend = Server.userExists(name);
		rep.setTitle("consultarPosts");
		rep.setPosts(friend.getPosts().toArray(new Post[0]));
		
		System.out.println("Sending posts list, size: "+friend.getPosts().size());

		return rep;
	}

	public static Reply newPost(Request req) {
		Post tmp = req.getPost();
		long t = tmp.getCreated().getTime()- new Date().getTime();
		Timer tim = new Timer();
		
		if(t>0){
			Server.delayed.offer(req);
			tim.schedule(new addNewPost(req,tim,true),t);
		}
		else{
			tim.schedule(new addNewPost(req,tim,true), 0);
		}
		Server.saveFiles();
		return null;
	}

	public static Reply editPost(Request req) {
		Post post = req.getPost();
		User user = userExists(req.getUsername());
		Post tmp = user.findPost(post.getID());
		tmp.editPost(post,req.getImageBytes());
		Server.saveFiles();
		
		Server.noti("User "+ user.getName()+ " edited post " + post.getID(),user);
		return null;
	}

	public static Reply deletePost(Request req) {
		int id = req.getID();
		User user = userExists(req.getUsername());
		Post post = user.findPost(id);
		user.removePost(post);

		//Remove from fb
		FacebookRestClient fb = user.getFacebookClient();
		if(fb!=null){
			if(post.getFbPostId()!=null){
				if(fb.removeFacebookPost(post.getFbPostId())){
					//some code
				}else{
					Server.notiUser("Não pode apagar um Post criado no Facebook!",user);
				}
			}
		}
		Server.saveFiles();

		Server.noti("User " + user.getName() + " deleted post " + post.getID(),user);
		return null;
	}

	public static Reply sendMessage(Request req) {
		User receiver;
		receiver = Server.userExists(req.getName());
		User user = userExists(req.getUsername());

		if(receiver!=null){
			req.getMessage().setSender(user.getName());
			receiver.addMessage(req.getMessage());
			Server.notiUser("User "+ user.getName()+ " sent you a message",receiver);
			System.out.println("Message from "+user.getName()+" sent to user "+receiver.getName());
		} else{
			System.out.println("Message not sent, user "+req.getName()+" doesnt exist");
		}
		Server.saveFiles();
		return null;
	}

	public static Reply listMessages(Request req){
		Reply rep = new Reply();
		User user = userExists(req.getUsername());
		rep.setTitle("listMessages");
		rep.setMessages(user.getMessages().toArray(new Message[0]));
		System.out.println("Listed messages from user "+req.getUsername()+", size: "+user.getMessages().size());
		return rep;
	}

	public static Reply replyPost(Request req) {
		User u = Server.userExists(req.getName());
		Post p = u.findPost(req.getID());
		User user = userExists(req.getUsername());
		p.addComment(req.getPass());

		//Reply on facebook
		FacebookRestClient fb = user.getFacebookClient();
		if(fb!=null){
			fb.addFacebookComment(p.getFbPostId(),req.getPass());
		}
		Server.saveFiles();

		Server.notiUser("User "+user.getName()+" added a comment to your post with id: "+req.getID(),u);
		System.out.println("User "+user.getName()+" added a comment to "+u.getName()+" post with id: "+req.getID());
		return null;
	}

	public static Reply addFriend(Request req) {
		Boolean added = false;
		String name;
		User tmp;
		Reply rep = new Reply();
		User user = userExists(req.getUsername());
		name = req.getName();

		if((tmp=Server.userExists(name))!=null) added = !user.isFriend(tmp);
	
		if(added){
			rep.setReport("Amigo adicionado\n");
			Server.noti("User "+ user.getName()+ " added friend " + req.getName(),user);
			user.addFriend(tmp);
			tmp.addFollower(user);
		}
		else rep.setReport("Amigo nao adicionado\n");

		rep.setTitle("adicionarAmigo");
		rep.setBool(added);
		Server.saveFiles();
		return rep;
	}

	public static Reply removeFriend(Request req) {
		User user = userExists(req.getUsername());
		User tmp = user.findFriend(req.getName());
		if(tmp!=null){
			user.removeFriend(tmp);
			tmp.removeFollower(user);
			System.out.println("User "+user.getName()+ " removed " + req.getName() + " as a friend");
			Server.noti("User "+ user.getName()+ " removed friend " + req.getName(),user);
		}
		Server.saveFiles();
		return null;
	}

	public static Reply listOnlineUsers() {
		ArrayList<String> users = new ArrayList<>();
		User tmp;
		Iterator<User> it = Server.online.iterator();
		while(it.hasNext()){
			tmp = it.next();
			if(tmp.getLogged()==false){
			}
			else if(!users.contains(tmp.getName())){
				users.add(tmp.getName());
			}
		}
		Reply rep = new Reply();
		rep.setTitle("listOnlineUsers");
		rep.setUsers(users.toArray(new String[0]));
		return rep;
	}

	public static Reply logout(Request req) {
		User user = userExists(req.getUsername());
		user.setLogged(false);
		user.setFacebookRestClient(null);
		Server.removeOnlineUser(user);
		Reply rep = new Reply();
		rep.setTitle("Logout");
		rep.setBool(true);
		rep.setReport("Logged out");
		System.out.println(user.getName() + " logged out");
		Server.noti("User "+ user.getName()+ " logged out",user);
		Server.saveFiles();
		return rep;
	}

}


class CheckFacebookComments extends Thread{
	public void run(){
		while(true){
			try{
				Thread.currentThread().sleep(5000);
			}catch (Exception e){}

			Iterator<User> it;
			it = Server.online.iterator();
			User tmp;

			while(it.hasNext()){
				tmp = it.next();
				//Receber array de novos posts se existirem
				ArrayList<Post> psts =  new ArrayList<Post>();
				psts = tmp.updateFacebookPosts();

				//Adiciona-los a conta do user
				if(psts.size()>0){
					for (int i =0;i<psts.size();i++){
						//preparar request
						Request req = new Request();
						req.setPost(psts.get(i));
						System.out.println("A adicionar um novo post do utilizador:" + tmp.getName());
						req.setName(tmp.getName());
						//adicionar no timer
						Timer tim = new Timer();
						tim.schedule(new addNewPost(req,tim,false), 0);
					}
				}
			}
		}
	}
}

class ConfirmThread extends Thread {
	private String ip;

	public ConfirmThread( String ip ){
		this.ip = ip;
		this.start();
	}
	public void run(){
		DatagramSocket socket = null;
		try{
			socket = new DatagramSocket();

			while(true){
				String texto="ping";
				byte[] m =texto.getBytes();
				// DatagramPacket request = new DatagramPacket(m,m.length);
				InetAddress address = InetAddress.getByName(ip);

				// socket.receive(request);

				DatagramPacket ping   = new DatagramPacket(m,m.length, address , Server.UDP_PORT);
				socket.send(ping);
				Thread.sleep(1000);
			}
		} catch(SocketTimeoutException e){			
		} catch (SocketException e){
			System.out.println("Socket2: " + e.getMessage());
		} catch (IOException e){
			System.out.println("IO: " + e.getMessage());
		} catch (InterruptedException e){
		}
	}
}

class ShutdownHook {
	public void attachShutDownHook(){
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override	
			public void run() {
				
			}
		});
	}
}

class addNewPost extends TimerTask{
	Post pst;
	User usr;
	Timer tim;
	Request req;
	Boolean postToFace;
	public addNewPost(Request req,Timer tim,boolean postToFace){
		this.pst = req.getPost();
		this.usr = Server.userExists(req.getName());
		this.tim = tim;
		this.req = req;
		this.postToFace = postToFace;
	}

	public void run(){
		Server.delayed.remove(req);
		pst.setUser(usr.getName());
		usr.addPost(pst);
		if(req.getImageBytes()!=null){
			pst.setImageBytes(req.getImageBytes());
		}

		FacebookRestClient fb = usr.getFacebookClient();
		if (fb != null){
			//Śystem.out.println(tmp.addFacebookPost(pst.getPost()))
			if(postToFace){pst.setFbPostId(fb.addFacebookPost(pst.getPost()));}
		}
		Server.saveFiles();
		Server.noti("User "+usr.getName()+" added a new post",usr);
		System.out.println("Post added");
		tim.cancel();
		
	}
}


class Connection implements Runnable{
	ObjectInputStream ois;
	ObjectOutputStream oos;
	Socket clientSocket;
	Boolean logged_in;
	User user;
	Request req;
	ConcurrentHashMap<Integer,Integer> served = new ConcurrentHashMap<Integer,Integer>();

	public Connection(Socket clientSocket) throws java.rmi.RemoteException{
		try{
			this.oos = new ObjectOutputStream(clientSocket.getOutputStream());
			this.ois = new ObjectInputStream(clientSocket.getInputStream());
			this.clientSocket = clientSocket;
			this.logged_in = false;
		} catch (IOException e){
			System.out.println("IO: " + e.getMessage());
		}
	}

	@Override
	public void run(){
		while(true){
			try{
				req = (Request)ois.readObject();
				oos.flush();
				oos.reset();
				System.out.println(req.getOrder());
				if(served.get(req.getOrderID())==null){
					switch(req.getOrder()){
						case 0: login(); break;
						case 1: registar(); break;
						case 2: listarAmigos(); break;
						case 3: consultarPosts(); break;
						case 4: novoPost(); break;
						case 5: editarPost(); break;
						case 6: apagarPost(); break;
						case 7: responderPost(); break;
						case 8: enviarMensagem(); break;
						case 9: consultarMensagens(); break;
						case 10: adicionarAmigo(); break;
						case 11: removerAmigo(); break;
						case 12: listOnlineUsers(); break;
						case 13: logout(); break;
						case 14: silentLogin(); break;
						default: break;
					}
				}
			} catch (Exception e){
				user.removeStream(oos);
				user.setLogged(false);
				System.out.println("IO: " + e.getMessage());
				Server.logged.remove(this);
				System.out.println("Socket Removed");
				return;
			}
		}
	}

	public void silentLogin() {
		this.user = Server.userExists(req.getName());
		this.user.setStream(oos);
		this.user.setLogged(true);
		Server.addOnlineUser(this.user);
	}

	public void write(Reply rep,ObjectOutputStream oos){
		try{
			oos.writeObject(rep);
		} catch (Exception e){
			e.printStackTrace();
		}
	}

	public void login() {
		System.out.println("liggin in");
		Reply rep = Server.login(req);
		this.logged_in = rep.getBool();
		/*Login sucess*/
		if(this.logged_in){
			user = rep.getUser();
			user.setStream(oos);
		}
		else{
			user = null;
		}
		this.write(rep,this.oos);
	}

	public void registar() {
		Reply rep = Server.register(req);
		this.write(rep,this.oos);
	}

	public void listarAmigos() {
		Reply rep = Server.listFriends(req);
		this.write(rep,this.oos);
	}

	public void consultarPosts() {
		Reply rep  = Server.listPosts(req);
		this.write(rep,this.oos);
	}

	public void novoPost() {
		Reply rep = Server.newPost(req);
		addServed(req.getOrderID());
		Reply ack = new Reply();
		ack.setTitle("orderAck");
		ack.setOrderID(req.getOrderID());
		this.write(ack,this.oos);
	}

	public void editarPost() {
		Reply rep = Server.editPost(req);
		addServed(req.getOrderID());
		Reply ack = new Reply();
		ack.setTitle("orderAck");
		ack.setOrderID(req.getOrderID());
		this.write(ack,this.oos);
	}

	public void apagarPost() {
		Reply rep = Server.deletePost(req);
		addServed(req.getOrderID());
		Reply ack = new Reply();
		ack.setTitle("orderAck");
		ack.setOrderID(req.getOrderID());
		this.write(ack,this.oos);
	}

	public void enviarMensagem() {
		Reply rep = Server.sendMessage(req);
		addServed(req.getOrderID());
		Reply ack = new Reply();
		ack.setTitle("orderAck");
		ack.setOrderID(req.getOrderID());
		this.write(ack,this.oos);
	}

	public void consultarMensagens() {
		Reply rep = Server.listMessages(req);
		this.write(rep,this.oos);
	}

	public void responderPost() {
		Reply rep = Server.replyPost(req);
		addServed(req.getOrderID());
		Reply ack = new Reply();
		ack.setTitle("orderAck");
		ack.setOrderID(req.getOrderID());
		this.write(ack,this.oos);
	}

	public void adicionarAmigo() {
		Reply rep = Server.addFriend(req);
		addServed(req.getOrderID());
		this.write(rep,this.oos);
		Reply ack = new Reply();
		ack.setTitle("orderAck");
		ack.setOrderID(req.getOrderID());
		this.write(ack,this.oos);
	}

	public void removerAmigo() {
		Reply rep = Server.removeFriend(req);
		addServed(req.getOrderID());
		Reply ack = new Reply();
		ack.setTitle("orderAck");
		ack.setOrderID(req.getOrderID());
		this.write(ack,this.oos);
	}

	public void listOnlineUsers() {
		Reply rep = Server.listOnlineUsers();
		this.write(rep,this.oos);
	}

	public void logout() {
		Reply rep = Server.logout(req);
		logged_in = false;
		user.removeStream(oos);
		this.write(rep,this.oos);
		this.user = null;
	}

	public void addServed(int n){
		served.putIfAbsent(n,n);
	}

}

/*class Notify implements Runnable {

}*/
