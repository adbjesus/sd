/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import java.io.*;
import java.net.*;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.*;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import rmi.*;
import db.*;
import java.awt.Image;

public class Client
extends java.rmi.server.UnicastRemoteObject
implements ClientInterface{
    int orderID = 0;
	newGUI g;
	Socket s = null;
	int port;
	ObjectOutputStream oos;
	Thread reader;
	Post []pst;
	Message []msg;
	byte[] imageBytes = null;
    Writer writer;
    boolean connectionType = false; //  true-RMI    |    false-TCP Only
    read readerRMI;
    String ip_normal;
    String ip_backup;
    ConcurrentLinkedQueue<Request> buffer;
    Boolean down;
    WriterRMI writerRMI;
    Connect connect;
    String username = "";
    String ascii;

    public Client(newGUI g) throws java.rmi.RemoteException{
        this.g = g;
        this.port = 7000;
        this.writer = null;
        this.readerRMI = null;
        this.s = null;
        this.buffer = null;
        this.down = false;
        this.connect = new Connect(this,g);
        connect.start();
    }

    public void noti(Reply rep) throws java.rmi.RemoteException{
        readerRMI.escolheOpcao(rep);
    }

    public void ack(Reply rep) throws java.rmi.RemoteException{
        System.out.println("RECEIVED ACK");
        writerRMI.removeRequest(rep);
    }

    public void setImageBytes(byte[] imageBytes){
        this.imageBytes=imageBytes;
    }

    public void register(String ip, String username, String password){
        ip_normal = ip;
        Request temp = new Request();
        temp.setOrder(1);
        temp.setName(username);
        temp.setPass(password);
        temp.setOrderID(-1);
        connect.connect();

        if(down){
            g.setNotification("Connection Lost ... Trying to reconnect\n");
            return;
        }
        
        if(this.connectionType==false){
            writer.offerRequest(temp);
            
        }else{
            temp.setClientInterface(this);
            writerRMI.offerRequest(temp);
        }
    }

    public void logIn(String ip, String username, String password,FacebookRestClient tmp)
    {
        ip_normal = ip;
        Request temp = new Request();
        temp.setFacebookRestClient(tmp);
        temp.setOrder(0);
        temp.setName(username);
        temp.setPass(password);
        temp.setOrderID(-1);

        this.username = username;

        connect.connect();

        if(down){
            g.setNotification("Connection Lost ... Trying to reconnect\n");
            return;
        }
        if(this.connectionType==false){
            writer.offerRequest(temp);           
        }else{
            temp.setClientInterface(this);
            writerRMI.offerRequest(temp);

        }
    }

    public void logout(){ 
        Request temp = new Request();
        temp.setOrder(13);
        temp.setOrderID(-1);
        temp.setUsername(username);
        if(down){
            g.setNotification("Connection Lost ... Trying to reconnect\n");
            return;
        }
        if(this.connectionType==false){
            writer.offerRequest(temp);
            
        }else{
            temp.setClientInterface(this);
            writerRMI.offerRequest(temp);
        }  
    }

    public void enviaAmigo(String friend) {
        Request temp = new Request();
        temp.setOrder(3);
        temp.setName(friend);
        temp.setOrderID(-1);
        temp.setUsername(this.username);


        if(this.connectionType==false){
            writer.offerRequest(temp);
            
        }else{
            temp.setClientInterface(this);
            writerRMI.offerRequest(temp);
        }
    }

    public void listarAmigos(){
        Request temp = new Request();
        temp.setOrder(2);
        temp.setOrderID(-1);
        temp.setUsername(this.username);

        if(down){
            g.setNotification("Connection Lost ... Trying to reconnect\n");
            return;
        }
        if(this.connectionType==false){
            writer.offerRequest(temp);
            
        }else{
            temp.setClientInterface(this);
            writerRMI.offerRequest(temp);
        }
    }

    public void consultarOwnPosts(){
        Request temp = new Request();
        temp.setOrder(3);
        temp.setName(g.Txt_User.getText());
        temp.setOrderID(-1);
        this.pst=null;
        temp.setUsername(this.username);
        if(down){
            g.setNotification("Connection Lost ... Trying to reconnect\n");
            return;
        }
        if(this.connectionType==false){
            writer.offerRequest(temp);
            
        }else{
            temp.setClientInterface(this);
            writerRMI.offerRequest(temp);
        }  
    }

    public void addReply(String s, String friend, int id){
        Request temp = new Request();
        temp.setOrder(7);
        temp.setName(friend);
        temp.setPass(s);
        temp.setID(id);
        temp.setOrderID(this.orderID++);
        temp.setUsername(this.username);

        
        if(this.connectionType==false){
            writer.offerRequest(temp);
            
        }else{
            temp.setClientInterface(this);
            writerRMI.offerRequest(temp);
        }
    }

    public void showPost(String n){
        String temp=n;

        temp = temp.substring(0, temp.indexOf(" "));

        if(g.Lbl_Post.getText().compareTo("Sobre:")==0){
            for(int z=0;z<this.msg.length;z++){
                if(temp.compareTo(Integer.toString(msg[z].getID()))==0){
                    g.Txt_Tema.setText(msg[z].getTitle());
                    g.Txt_Corpo.setText("De: " + msg[z].getSender() + "\n");
                    g.Txt_Corpo.append("Corpo: " + msg[z].getMessage());
                }
            }
        }else if(g.Btn_Ordem.getText().compareTo("Reply")==0 || g.Btn_Ordem.getText().compareTo("Editar")==0){
            int z;
            for(z=0;z<this.pst.length;z++){
                if(temp.compareTo(Integer.toString(pst[z].getID()))==0){
                    g.Txt_Tema.setText(pst[z].getTitle());
                    g.Txt_Corpo.setText(pst[z].getPost());
                    Iterator<String> it = pst[z].getComments().iterator();
                    if(it.hasNext()){
                        g.Txt_Corpo.append("\n\nComments:");
                    }
                    while(it.hasNext()){
                        g.Txt_Corpo.append("\n"+it.next()+"\n");
                    }
                    break;
                }
            }
            if(pst[z].getImage() != null){
                this.ascii = pst[z].getImage();
                g.Btn_Imagem.setEnabled(true);
            }else{
                g.Btn_Imagem.setEnabled(false);
            }
        }
    }

    public void adicionarAmigo(String friend){
        Request temp = new Request();
        temp.setOrder(10);
        temp.setName(friend);
        temp.setOrderID(this.orderID++);
        temp.setUsername(this.username);
       
        if(this.connectionType==false){
            writer.offerRequest(temp);
            
        }else{
            temp.setClientInterface(this);
            writerRMI.offerRequest(temp);
        }
    }

    public void removerAmigo(String friend){
        Request temp = new Request();     
        temp.setOrder(11);
        temp.setName(friend);
        temp.setOrderID(this.orderID++);
        temp.setUsername(this.username);
        
        if(this.connectionType==false){
            writer.offerRequest(temp);
            
        }else{
            temp.setClientInterface(this);
            writerRMI.offerRequest(temp);
        } 
    }

    public void enviaPost(String tema,String post, Date date){
        Request temp = new Request();
        temp.setOrder(4);
        temp.setPost(new Post(tema,post,date));
        temp.setOrderID(this.orderID++);
        temp.setName(g.Txt_User.getText());
        temp.setUsername(this.username);
        temp.setImageBytes(this.imageBytes);
        this.imageBytes = null;

        if(this.connectionType==false){
            writer.offerRequest(temp);
            
        }else{
            temp.setClientInterface(this);
            writerRMI.offerRequest(temp);
        }
    }

    public void enviaMensagem(String tema,String message,String receiver, Date date){
        Request temp = new Request();
        temp.setOrder(8);
        temp.setMessage(new Message(tema,message,date));
        temp.setName(receiver);
        temp.setOrderID(this.orderID++);
        temp.setUsername(this.username);
        
        if(this.connectionType==false){
            writer.offerRequest(temp);
            
        }else{
            temp.setClientInterface(this);
            writerRMI.offerRequest(temp);
        }
    }

    public void editarPost(String tema, String post,String st, Date date){
        Request temp = new Request();
        temp.setOrder(5);
        Post tmp = new Post(tema,post,date);
        st = st.substring(0, st.indexOf(" "));
        tmp.setID(Integer.parseInt(st));
        temp.setPost(tmp);
        temp.setOrderID(this.orderID++);
        temp.setUsername(this.username);
        temp.setImageBytes(this.imageBytes);
        this.imageBytes = null;

        if(this.connectionType==false){
            writer.offerRequest(temp);
        }else{
            temp.setClientInterface(this);
            writerRMI.offerRequest(temp);
        }
    }

    public void verMensagem(){
        Request temp = new Request();
        temp.setOrder(9);
        temp.setOrderID(-1);
        temp.setUsername(this.username);
        if(down){
            g.setNotification("Connection Lost ... Trying to reconnect\n");
            return;
        }
        if(this.connectionType==false){
            writer.offerRequest(temp);
            
        }else{
            temp.setClientInterface(this);
            writerRMI.offerRequest(temp);
        }
    }

    public void apagarPost(String st){
        Request temp = new Request();
        temp.setOrder(6);
        st = st.substring(0, st.indexOf(" "));
        temp.setID(Integer.parseInt(st));
        temp.setOrderID(this.orderID++);
        temp.setUsername(this.username);

        
        if(this.connectionType==false){
            writer.offerRequest(temp);
            
        }else{
            temp.setClientInterface(this);
            writerRMI.offerRequest(temp);
        }
    }

    public void listarOnline(){
        Request temp = new Request();
        temp.setOrder(12);
        temp.setOrderID(-1);
        temp.setUsername(this.username);
        if(down){
            g.setNotification("Connection Lost ... Trying to reconnect\n");
            return;
        }
        if(this.connectionType==false){
            writer.offerRequest(temp);
            
        }else{
            temp.setClientInterface(this);
            writerRMI.offerRequest(temp);
        }
    }
}

class read implements Runnable{
    private Client c;
    private ObjectInputStream ois;
    private String s;
    private Socket sck;
    private newGUI g;
    private String[] usr;
    private Reply tmp;
    private Post pst;
    private Post[] apst;

    public read(Socket s,newGUI g,Client c){
        this.c = c;
        this.g = g;

        this.sck = s;
        if(sck!=null){
            try{
                this.ois = new ObjectInputStream(sck.getInputStream());
            } catch (IOException e){
                e.printStackTrace();
            }
        }
        else this.ois=null;
    }

    @Override
    public synchronized void run(){
        
            while(true){
                try{
                    tmp = (Reply)ois.readObject();
                    escolheOpcao(tmp);
                } catch(IOException e){
                    c.down = true;
                    c.connect.noti();
                    return;
                } catch(ClassNotFoundException e){
                    g.setNotification("Exception: " + e.getMessage() + "\n");
                }
            }
         
    }

    public void escolheOpcao(Reply tmp){
        s = tmp.getTitle();

        if(s.compareTo("Notify")==0)                notificar(tmp);
        else if(s.compareTo("Login")==0)            login(tmp);
        else if(s.compareTo("Registar")==0)         notificar(tmp);
        else if(s.compareTo("listarAmigos")==0)     listarAmigos(tmp);
        else if(s.compareTo("consultarPosts")==0)   consultarPosts(tmp);
        else if(s.compareTo("adicionarAmigo")==0)   notificar(tmp);
        else if(s.compareTo("listMessages")==0)     listMessages(tmp);
        else if(s.compareTo("listOnlineUsers")==0)  listOnlineUsers(tmp);
        else if(s.compareTo("Logout")==0)           logout(tmp);
        else if(s.compareTo("orderAck")==0)         c.writer.removeRequest(tmp);
    }

    public void logout(Reply tmp){
        if(tmp.getBool()){
            g.setNotification(tmp.getReport()+"\n");
            g.logoutPerfomed();
        }
    }

    public void login(Reply tmp){
        if(tmp.getBool()){
            g.setNotification(tmp.getReport());
            g.loginPerfomed();
        }else{
            g.setNotification(tmp.getReport());
            c.username = "";
        }
    }

    public void notificar(Reply tmp){
        g.setNotification(tmp.getReport()+"\n");
    }

    public void listarAmigos(Reply tmp){
        usr = tmp.getUsers();
        if(usr.length==0){
            g.setCmb_Amigos("Não tem Amigos");
            return;
        }

        for(int z=0;z<usr.length;z++){
            g.setCmb_Amigos(usr[z]);
        }
    }

    public void consultarPosts(Reply tmp){
        c.pst = tmp.getPosts();
        if(c.pst.length==0){
            g.setCmb_Posts("Não tem Posts");	
            return;				
        }

        for(int z=0;z<c.pst.length;z++){
            g.setCmb_Posts( Integer.toString(c.pst[z].getID()) + " " + c.pst[z].getTitle());
        }
    }

    public void listMessages(Reply tmp){
        c.msg = tmp.getMessages();

        if(c.msg.length==0){
            g.setCmb_Posts("Não tem Mensagens");
            g.Cmb_Posts.setEnabled(false);
            g.Btn_Posts.setEnabled(false);
            return;
        }

        for(int z=0;z<c.msg.length;z++){
            g.setCmb_Posts(Integer.toString(c.msg[z].getID()) + " " + c.msg[z].getTitle());
        }
    }

    public void listOnlineUsers(Reply tmp){
        usr = tmp.getUsers();
        g.setNotification("Online Users:\n");

        for(int z=0;z<usr.length;z++){
            g.setNotification(usr[z]+"\n");
        }
    }

}


class Writer extends Thread {
    ConcurrentLinkedQueue<Request> requests;
    ObjectOutputStream oos;
    Request tmp;
    Client c;
    newGUI g;
    Socket sck;
    Boolean down;
    
    Writer(Client c, Socket s,newGUI g){
        this.sck = s;
        if(this.sck!=null){
            try {
                this.oos = new ObjectOutputStream(sck.getOutputStream());
            } catch (IOException ex) {      
            }
        }
        requests = new ConcurrentLinkedQueue<>();
        this.g=g;
        this.c = c;
        this.down = false;
        this.start();
    }
    
    public synchronized void run(){
        try{
            while(true){

                Iterator<Request> it = requests.iterator();
                while(it.hasNext()){
                    tmp = it.next();
                    System.out.println("Processing request with order: " + tmp.getOrder() +" and orderID: "+tmp.getOrderID());
                    oos.writeObject(tmp);
                    //oos.flush();
                    oos.reset();
                    if(tmp.getOrder() == 0 | tmp.getOrder() == 1 |tmp.getOrder() == 2| tmp.getOrder() == 3 | tmp.getOrder() == 9 | tmp.getOrder() == 12 | tmp.getOrder() == 13 | tmp.getOrder() == 14)
                        requests.remove(tmp); 
                }
                this.wait();
            }
            
        } catch(InterruptedException | IOException e){
            c.buffer = this.requests;
            this.requests = null;
            this.down = true;
        }
    }
    
    public synchronized void offerRequest(Request req){
        if(this.down){
            c.buffer.offer(req);
            return;
        }
        else{
            this.requests.offer(req);   
            this.notify();
        }
    }

    public synchronized void setBuffer(ConcurrentLinkedQueue<Request> buf){
        if(buf!=null){
            this.requests.addAll(buf);
            this.notify();
        }
    }
    
    public void removeRequest(Reply rep){
        Request remover;
        Iterator<Request> it = requests.iterator();
                while(it.hasNext()){
                    remover = it.next();
                    if(rep.getOrderID() == remover.getOrderID()){
                        requests.remove(remover);
                        break;
                    }
                }
    }
}

class WriterRMI extends Thread {
    ConcurrentLinkedQueue<Request> requests;
    Request tmp;
    RMIServerInterface server;
    Client c;
    newGUI g;
    Boolean down;
    
    WriterRMI(Client c, newGUI g) throws RemoteException,NotBoundException,MalformedURLException{
        this.server = (RMIServerInterface) Naming.lookup("rmi://" + c.ip_normal + ":8000/rmiServer");
        this.server.subscribe(c);
        requests = new ConcurrentLinkedQueue<>();
        this.g=g;
        this.c = c;
        this.down = false;
        this.start();
    }
    
    public synchronized void run(){
        Reply rep;
        Iterator<Request> it;
        try{
            while(true){

                it = requests.iterator();
                while(it.hasNext()){
                    tmp = it.next();
                    rep = null;
                    System.out.println("Processing request with order: " + tmp.getOrder() +" and orderID: "+tmp.getOrderID());
                    switch(tmp.getOrder()){
                        case 0: rep = this.server.login(tmp); break;
                        case 1: rep = this.server.registar(tmp); break;
                        case 2: rep = this.server.listarAmigos(tmp); break;
                        case 3: rep = this.server.consultarPosts(tmp); break;
                        case 4: this.server.novoPost(tmp); break;
                        case 5: this.server.editarPost(tmp); break;
                        case 6: this.server.apagarPost(tmp); break;
                        case 7: this.server.responderPost(tmp); break;
                        case 8: this.server.enviarMensagem(tmp); break;
                        case 9: rep = this.server.consultarMensagens(tmp); break;
                        case 10: rep = this.server.adicionarAmigo(tmp); break;
                        case 11: this.server.removerAmigo(tmp); break;
                        case 12: rep = this.server.listOnlineUsers(tmp); break;
                        case 13: rep = this.server.logout(tmp); break;
                        case 14: this.server.silentLogin(tmp); break;
                        default: break;
                    }
                    if(rep!=null)
                        this.c.readerRMI.escolheOpcao(rep);
                    if(tmp.getOrder() == 0 | tmp.getOrder() == 1 |tmp.getOrder() == 2| tmp.getOrder() == 3 | tmp.getOrder() == 9 | tmp.getOrder() == 12 | tmp.getOrder() == 13 | tmp.getOrder() == 14)
                        requests.remove(tmp); 
                }
                this.wait();
            }
            
        } catch(InterruptedException | RemoteException e){
            it = requests.iterator();
            while(it.hasNext()){
                tmp = it.next();
                if(tmp.getOrder() == 0 | tmp.getOrder() == 1 |tmp.getOrder() == 2| tmp.getOrder() == 3 | tmp.getOrder() == 9 | tmp.getOrder() == 12 | tmp.getOrder() == 13 | tmp.getOrder() == 14)
                    requests.remove(tmp);
            }
            c.buffer = this.requests;
            this.requests = null;
            this.down = true;
            c.connect.noti();
        }
    }
    
    public synchronized void offerRequest(Request req){
        if(this.down){
            c.buffer.offer(req);
            return;
        }
        else{
            this.requests.offer(req);   
            this.notify();
        }
    }

    public synchronized void setBuffer(ConcurrentLinkedQueue<Request> buf){
        if(buf!=null){
            this.requests.addAll(buf);
            this.notify();
        }
    }
    
    public void removeRequest(Reply rep){
        Request remover;
        Iterator<Request> it = requests.iterator();
                while(it.hasNext()){
                    remover = it.next();
                    if(rep.getOrderID() == remover.getOrderID()){
                        requests.remove(remover);
                        break;
                    }
                }
    }
}

class Connect extends Thread{
    Client c;
    newGUI g;
    public Connect(Client c,newGUI g){
        this.c = c;
        this.g = g;
    }

    public synchronized void run(){
        while(true){
            try{
                this.wait();
                this.connect();  
            } catch (InterruptedException e){
                e.printStackTrace();
            }
        }
        
    }

    public void connect(){
        if(this.c.down){
            if(this.g.Rb_RMI.isSelected())
                reconnectRMI();
            else
                reconnectTCP();
        }
        else{
            if(this.g.Rb_RMI.isSelected() && !c.connectionType)
                connectRMI();
            else if(!this.g.Rb_RMI.isSelected() && (c.connectionType || c.s==null))
                connectTCP();
        }
    }

    public void connectTCP(){
        try{
            c.s = new Socket(c.ip_normal,c.port);
        } catch (java.net.UnknownHostException e){
            reconnectTCP();
        } catch (IOException e){
            reconnectTCP();
        }
        c.connectionType=false;
        c.orderID = 0;
        c.writer = new Writer(c,c.s,g);
        c.readerRMI = new read(c.s,g,c);
        c.reader = new Thread(c.readerRMI);
        c.reader.start();
    }
    public void connectRMI(){
        this.g.setNotification("Starting RMI Connection\n");
        c.connectionType=true;
        if(c.s!=null){            
            c.s = null;
        }
        c.orderID = 0;
        try{
            c.writerRMI = new WriterRMI(c,g);
        } catch (RemoteException | MalformedURLException | NotBoundException e){
            reconnectRMI();
        }
        c.readerRMI = new read(c.s,g,c);
        this.g.setNotification("RMI Connection Established...\n");
    }
    public void reconnectTCP(){
        c.down = true;
        c.connectionType=false;
        while(c.down){
            try{
                Thread.currentThread().sleep(1500);
            } catch (InterruptedException e){}
            System.out.print("Attempt to reconnect: ");
            try{
                c.s = new Socket(c.ip_normal,c.port);
                resetStreams();
                System.out.println("sucess");
                return;
            } catch (Exception e){
                System.out.println("fail");
            }    
        }
    }

    public void resetStreams(){
        try{
            c.readerRMI = new read(c.s,c.g,c);
            c.reader = new Thread(c.readerRMI);
            c.reader.start();
            c.writer = new Writer(c,c.s,c.g);
        } catch (Exception e){
            e.printStackTrace();
        }
        Request req = new Request();
        req.setName(g.Txt_User.getText());
        req.setOrder(14);
        req.setOrderID(-1);
        c.writer.offerRequest(req);
        c.writer.setBuffer(c.buffer);
        c.buffer=null;
        c.down = false;
    }

    public void reconnectRMI(){
        c.down = true;
        c.connectionType = true;
        while(c.down){
            try{
                Thread.currentThread().sleep(1500);
            } catch (InterruptedException e){}
            System.out.print("Attempt to reconnect: ");
            try{
                c.writerRMI = new WriterRMI(c,g);
                c.readerRMI = new read(c.s,g,c);
                Request req = new Request();
                req.setName(g.Txt_User.getText());
                req.setOrder(14);
                req.setOrderID(-1);
                req.setClientInterface(c);
                c.writerRMI.offerRequest(req);
                c.writerRMI.setBuffer(c.buffer);
                c.buffer = null;
                c.down = false;
                System.out.println("sucess");
                return;
            } catch (NotBoundException | MalformedURLException | RemoteException e){
                 System.out.println("fail");
            }
        }
    }

    public synchronized void noti(){
        this.notify();
    }

}